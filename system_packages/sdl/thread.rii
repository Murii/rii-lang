@extern
enum SDL_ThreadPriority {
    SDL_THREAD_PRIORITY_LOW,
    SDL_THREAD_PRIORITY_NORMAL,
    SDL_THREAD_PRIORITY_HIGH,
}

@extern struct SDL_Thread;
@extern struct SDL_atomic_t;
@extern struct SDL_cond;
@extern struct SDL_mutex;
@extern struct SDL_sem;

@extern typedef SDL_threadID   = ulong;
@extern typedef SDL_TLSID      = uint;
@extern typedef SDL_SpinLock   = int;
@extern typedef SDL_Threadfunction = func(data: void*): int;


@extern
func SDL_CreateThread(fn: SDL_Threadfunction, name: char const*, data: void*): SDL_Thread*;

@extern
func SDL_DetachThread(thread: SDL_Thread*);

@extern
func SDL_GetThreadID(thread: SDL_Thread*): SDL_threadID;

@extern
func SDL_GetThreadName(thread: SDL_Thread*): char const*;

@extern
func SDL_SetThreadPriority(priority: SDL_ThreadPriority): int;

@extern
func SDL_TLSCreate(): SDL_TLSID;

@extern
func SDL_TLSGet(id: SDL_TLSID): void*;

@extern
func SDL_TLSSet(id: SDL_TLSID, value: void const*, destructor: func(void*)): int;

@extern
func SDL_ThreadID(): SDL_threadID;

@extern
func SDL_WaitThread(thread: SDL_Thread*, status: int*);

@extern
func SDL_CondBroadcast(cond: SDL_cond*): int;

@extern
func SDL_CondSignal(cond: SDL_cond*): int;

@extern
func SDL_CondWait(cond: SDL_cond*, mutex: SDL_mutex*): int;

@extern
func SDL_CondWaitTimeout(cond: SDL_cond*, mutex: SDL_mutex*, ms: uint32): int;

@extern
func SDL_CreateCond(): SDL_cond*;

@extern
func SDL_CreateMutex(): SDL_mutex*;

@extern
func SDL_CreateSemaphore(initial_value: uint32): SDL_sem*;

@extern
func SDL_DestroyCond(cond: SDL_cond*);

@extern
func SDL_DestroyMutex(mutex: SDL_mutex*);

@extern
func SDL_DestroySemaphore(sem: SDL_sem*);

@extern
func SDL_LockMutex(mutex: SDL_mutex*): int;

@extern
func SDL_SemPost(sem: SDL_sem*): int;

@extern
func SDL_SemTryWait(sem: SDL_sem*): int;

@extern
func SDL_SemValue(sem: SDL_sem*): uint32;

@extern
func SDL_SemWait(sem: SDL_sem*): int;

@extern
func SDL_SemWaitTimeout(sem: SDL_sem*, ms: uint32): int;

@extern
func SDL_TryLockMutex(mutex: SDL_mutex*): int;

@extern
func SDL_UnlockMutex(mutex: SDL_mutex*): int;

@extern
func SDL_AtomicLock(lock: SDL_SpinLock*);

@extern
func SDL_AtomicUnlock(lock: SDL_SpinLock*);

@extern
func SDL_AtomicIncRef(a: SDL_atomic_t*);

@extern
func SDL_AtomicDecRef(a: SDL_atomic_t*): SDL_bool;

@extern
func SDL_AtomicAdd(a: SDL_atomic_t*, v: int): int;

@extern
func SDL_AtomicCAS(a: SDL_atomic_t*, oldval: int, newval: int): SDL_bool;

@extern
func SDL_AtomicCASPtr(a: void**, oldval: void*, newval: void*): SDL_bool;

@extern
func SDL_AtomicGet(a: SDL_atomic_t*): int;

@extern
func SDL_AtomicGetPtr(a: void**): void*;

@extern
func SDL_AtomicSet(a: SDL_atomic_t*, v: int): int;

@extern
func SDL_AtomicSetPtr(a: void**, v: void*): void*;

@extern
func SDL_AtomicTryLock(lock: SDL_SpinLock*): SDL_bool;

@extern
func SDL_CompilerBarrier();
