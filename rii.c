enum { MAX_SEARCH_PATHS = 256 };
const char *static_package_search_paths[MAX_SEARCH_PATHS];
const char **package_search_paths = static_package_search_paths;
int num_package_search_paths;

void add_package_search_path(const char *path) {
    printf("Adding package search path %s\n", path);
    package_search_paths[num_package_search_paths++] = str_intern(path);
}

void add_package_search_path_range(const char *start, const char *end) {
    char path[MAX_PATH];
    size_t len = CLAMP_MAX(end - start, MAX_PATH - 1);
    memcpy(path, start, len);
    path[len] = 0;
    add_package_search_path(path);
}

void init_package_search_paths(char *argv[]) {
	const char *riihome_var = getenv("RIIHOME");
	if (!riihome_var) {
		riihome_var = argv[3];
		if (!riihome_var) {
			printf("error: Set the environment variable RIIHOME to the Rii home directory (where system_packages is located)\n");
			exit(1);
		}
	}
    char path[MAX_PATH];
    path_copy(path, riihome_var);
    path_join(path, "system_packages");
    add_package_search_path(path);
    add_package_search_path(".");
    const char *riipath_var = getenv("RIIPATH");
    if (riipath_var) {
        const char *start = riipath_var;
        for (const char *ptr = riipath_var; *ptr; ptr++) {
            if (*ptr == ';') {
                add_package_search_path_range(start, ptr);
                start = ptr + 1;
            }
        }
        if (*start) {
            add_package_search_path(start);
        }
    }
}

void init_compiler(char *argv[]) {
    init_package_search_paths(argv);
    init_keywords();
    init_builtin_types();
    map_put(decl_note_names, declare_note_name, (void *)1);
}

int rii_main(int argc, char **argv) {
    mlc_init();

    ast_arena = xmalloc(sizeof(Arena));
    intern_arena = xmalloc(sizeof(Arena));

    gen_name_map = xmalloc(sizeof(Map));

    typeid_map = xmalloc(sizeof(Map));
    cached_ptr_types = xmalloc(sizeof(Map));
    cached_const_types = xmalloc(sizeof(Map));
    cached_array_types = xmalloc(sizeof(Map));
    cached_func_types = xmalloc(sizeof(Map));

    interns = xmalloc(sizeof(Map));

    resolved_sym_map = xmalloc(sizeof(Map));

    resolved_expected_type_map = xmalloc(sizeof(Map));

    resolved_type_map = xmalloc(sizeof(Map));

    package_map = xmalloc(sizeof(Map));
    current_package = xmalloc(sizeof(Package));

    decl_note_names = xmalloc(sizeof(Map));

    if (argc < 2 || strcmp(argv[1],"--help") == 0) {
        printf("Usage: %s <package> [<output-c-file>] [system-libraries-path]\n", argv[0]);
        return 1;
    }
    const char *package_name = argv[1];
    init_compiler(argv);

    builtin_package = import_package("builtin");
    if (!builtin_package) {
        printf("error: Failed to compile package 'builtin'.\n");
        return 1;
    }
    builtin_package->external_name = str_intern("");
    init_builtin_syms();
    Package *main_package = import_package(package_name);
    if (!main_package) {
        printf("error: Failed to compile package '%s'\n", package_name);
        return 1;
    }
    const char *main_name = str_intern("main");
    Sym *main_sym = get_package_sym(main_package, main_name, NULL);
    if (!main_sym) {
        printf("error: No 'main' entry point defined in package '%s'\n", package_name);
        return 1;
    }
    main_sym->external_name = main_name;
    resolve_package_syms(builtin_package);
    resolve_package_syms(main_package);
    // for (int i = 0; i < buf_len(package_list); i++) {
    //     resolve_package_syms(package_list[i]);
    // }
    finalize_reachable_syms();
    printf("Compiled %d symbols in %d packages\n", (int)buf_len(reachable_syms), (int)buf_len(package_list));
    char c_path[MAX_PATH];
    if (argc >= 3) {
        path_copy(c_path, argv[2]);
    } else {
        snprintf(c_path, sizeof(c_path), "out_%s.c", package_name);
    }
    printf("Generating %s\n", c_path);
    gen_all();
    const char *c_code = gen_buf;
    gen_buf = NULL;
    if (!write_file(c_path, c_code, buf_len(c_code))) {
        printf("error: Failed to write file: %s\n", c_path);
        return 1;
    }


    mlc_free(package_map->keys);
    mlc_free(package_map->vals);
    mlc_free(package_map);

    mlc_free(current_package);

    mlc_free(resolved_type_map->keys);
    mlc_free(resolved_type_map->vals);
    mlc_free(resolved_type_map);

    mlc_free(resolved_expected_type_map->keys);
    mlc_free(resolved_expected_type_map->vals);
    mlc_free(resolved_expected_type_map);

    mlc_free(resolved_sym_map->keys);
    mlc_free(resolved_sym_map->vals);
    mlc_free(resolved_sym_map);

    mlc_free(gen_name_map->keys);
    mlc_free(gen_name_map->vals);
    mlc_free(gen_name_map);

    mlc_free(interns->keys);
    mlc_free(interns->vals);
    mlc_free(interns);

    mlc_free(decl_note_names->keys);
    mlc_free(decl_note_names->vals);
    mlc_free(decl_note_names);

    mlc_free(typeid_map->keys);
    mlc_free(typeid_map->vals);
    mlc_free(typeid_map);

    mlc_free(cached_ptr_types->keys);
    mlc_free(cached_ptr_types->vals);
    mlc_free(cached_ptr_types);

    if (cached_const_types->keys != NULL) {
        mlc_free(cached_const_types->keys);
        mlc_free(cached_const_types->vals);
    }
    mlc_free(cached_const_types);

    if (cached_array_types->keys != NULL) {
        mlc_free(cached_array_types->keys);
        mlc_free(cached_array_types->vals);
    }
    mlc_free(cached_array_types);

    if (cached_func_types->keys != NULL) {
        mlc_free(cached_func_types->keys);
        mlc_free(cached_func_types->vals);
    }
    mlc_free(cached_func_types);

    arena_free(ast_arena);
    arena_free(intern_arena);

    for (int i = 0; i < buf_len(package_list); i++) {
        mlc_free(package_list[i]->syms_map.keys);
        mlc_free(package_list[i]->syms_map.vals);
        buf_free(package_list[i]->syms);
        buf_free(package_list[i]->decls);
    }
    buf_free(package_list);
    buf_free(reachable_syms);
    buf_free(sorted_syms);

    buf_free(print_buf);

    for (size_t i = 0; i < buf_len(private_names_buffer); i++) {
        free(private_names_buffer[i]);
    }
    buf_free(private_names_buffer);

    printf("Bytes not freed: %zu\n", mlc_usage());
    mlc_destroy();
    return 0;
}
