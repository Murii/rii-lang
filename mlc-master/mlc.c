/*
Copyright (c) 2018, Muresan Vlad Mihail
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
must display the following acknowledgement:
This product includes software developed by Muresan Vlad Mihail.
4. Neither the name of Muresan Vlad Mihail nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY Muresan Vlad Mihail''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Muresan Vlad Mihail BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include "vector.h"

#include "mlc.h"

#define MLC_CHECK_FOR_ERRORS
#define MLC_CHECK_FOR_WARNINGS

typedef struct {
    const char* file; /* file in which the allocation happened */
    size_t line; /* line at which the allocation happened */
    size_t size; /* the size of allocated object */
    char free; /* 1-> free, 0-> not free yet */
    void* ptr; /* the actual object which is allocated */
} alloc_struct;


static vec_t* elements_v; /* holds all allocated structures */


void mlc_init()
{
    elements_v = vec_init(sizeof(void*));
}


void mlc_destroy()
{
    vec_destroy(elements_v);
}


static void add_structure(void* ptr, size_t obj_size, const char* file, unsigned line)
{
    alloc_struct* ns = malloc(sizeof(alloc_struct));
	if (!ns)
	{
		#ifdef MLC_CHECK_FOR_ERRORS
		fprintf(stderr, "Couldn't allocate mlc structure: %s, line %u\n", file, line);
		#endif
		return;
	}
    ns->file = file;
    ns->line = line;
    ns->size = obj_size;
    ns->ptr = ptr;
    ns->free = 0;

    vec_append(elements_v, ns);
}


static size_t has_structure(void* ptr)
{
    size_t i = 0;
    while (i < vec_size(elements_v))
    {
        if (((alloc_struct*)vec_get(elements_v, i))->ptr == ptr)
            return i;
        i++;
    }
    return 0;
}


void* _mlc_malloc(size_t size, const char* file, unsigned line)
{
	void* ptr = malloc(size);
	if (!ptr)
	{
		#ifdef MLC_CHECK_FOR_ERRORS
		fprintf(stderr, "Couldn't allocate: %s, line %u\n", file, line);
		#endif
		return NULL;
	}

    add_structure(ptr, size, file, line);
    return ptr;
}


void* _mlc_calloc(size_t nitems, size_t size, const char* file, unsigned line)
{
    void* ptr = calloc(nitems, size);
	if (!ptr)
	{
		#ifdef MLC_CHECK_FOR_ERRORS
		fprintf(stderr, "Couldn't allocate: %s, line %u\n", file, line);
		#endif
		return NULL;
	}

    add_structure(ptr, size, file, line);
    return ptr;
}


void* _mlc_realloc(void* ptr, size_t size, const char* file, unsigned line)
{
	void* _ptr = realloc(ptr, size);
	alloc_struct* as = NULL;
	size_t index = 0;
	if (!ptr)
	{
		#ifdef MLC_CHECK_FOR_ERRORS
		fprintf(stderr, "Couldn't allocate: %s, line %u\n", file, line);
		#endif
		return NULL;
	}

	index = has_structure(ptr);
	as = (alloc_struct*)vec_get(elements_v, index);

	if (!as)
	{
		#ifdef MLC_CHECK_FOR_WARNINGS
		fprintf(stdout, "Reallocating is happening on a NULL ptr %s %u\n", file, line);
		#endif
		add_structure(_ptr, size, file, line);
	}
	else
	{
		as->ptr = _ptr;
		as->size = size;
	}
	return _ptr;
}


void _mlc_free(void* ptr, const char* file, unsigned line)
{
	alloc_struct* as = NULL;
	size_t index = 0;
    if (ptr == NULL)
    {
        #ifdef MLC_CHECK_FOR_WARNINGS
        fprintf(stdout, "Couldn't free object: %s, line %u %s \n", file, line, " because it's NULL");
        #endif
        return;
    }


	index = has_structure(ptr);
    as = (alloc_struct*)vec_get(elements_v, index);
    if (!as)
    {
        #ifdef MLC_CHECK_FOR_WARNINGS
        fprintf(stderr, "Invalid free: %s, line %u\n", file, line);
        #endif
        return;
    }
	else
	{
        as->free = 1;
	}
    free(ptr);
}


size_t _mlc_size(void* ptr, const char* file, unsigned line)
{
    alloc_struct* _ptr = (alloc_struct*)vec_get(elements_v, has_structure(ptr));
    if (!ptr)
    {
        #ifdef MLC_CHECK_FOR_WARNINGS
        fprintf(stderr, "Bad pointer: %s, line %u\n", file, line);
        #endif
        return -1;
    }
    return _ptr->size;
}


size_t mlc_usage()
{
    size_t usage = 0;
    size_t i = 0;
    while (i < vec_size(elements_v))
    {
        alloc_struct* ptr = ((alloc_struct*)vec_get(elements_v, i));
        if (ptr->free == 0)
            usage +=  ptr->size;
        i++;
    }
    return usage;
}


void mlc_dump(FILE* file)
{
    alloc_struct* curr = NULL;
    size_t i = 0;
    size_t size = vec_size(elements_v);

    if (!file)
        file = stdout;

    if (size == 0)
        fprintf(file, "MLC: no memory leaks! \n");

    while (i < size)
    {
        curr = vec_get(elements_v, i);
        if (curr->free == 0)
            fprintf(file, "MLC: Unfreed: %p %s, line %zu\n", curr->ptr, curr->file, curr->line);
        i++;
    }
}

