/*
   Copyright (c) 2018, Muresan Vlad Mihail
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
   3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by Muresan Vlad Mihail.
   4. Neither the name of Muresan Vlad Mihail nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY Muresan Vlad Mihail''AS IS'' AND ANY
   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL Muresan Vlad Mihail BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */

#ifndef MLC_H
#define MLC_H

#include <stdio.h>

/*
 * 0.1.0 - initial commit
 * 0.1.1 - better management & performance
*/
#define MLC_VERSION 0.1.1


#define mlc_malloc(size) _mlc_malloc(size, __FILE__, __LINE__)
#define mlc_calloc(nitems, size) _mlc_calloc(nitems, size, __FILE__, __LINE__)
#define mlc_realloc(ptr, size) _mlc_realloc(ptr, size, __FILE__, __LINE__)
#define mlc_size(ptr) _mlc_size(ptr, __FILE__, __LINE__)
#define mlc_free(ptr) _mlc_free(ptr, __FILE__, __LINE__)

void mlc_init();
void mlc_destroy();
void* _mlc_malloc(size_t size, const char* file, unsigned line);
void* _mlc_calloc(size_t nitems, size_t size, const char* file, unsigned line);
void* _mlc_realloc(void* ptr, size_t size, const char* file, unsigned line);
void _mlc_free(void* ptr, const char* file, unsigned line);
size_t _mlc_size(void* ptr, const char* file, unsigned line);
size_t mlc_usage();
void mlc_dump(FILE* file);

#endif

